// Khai báo mongoose
const mongoose = require('mongoose');

// IMPORT  model  vào controller
const prizeHistoryModel = require('../model/prizeHistoryModel');
const userModel = require('../model/userModel');

// CREATE A PRIZE HISTORY
const createPrizeHistory = (request, response) => {
    // B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!bodyRequest.user) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Prize History is require"
        })
    }

    if (!bodyRequest.prize) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Prize History is require"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let createPrizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user: bodyRequest.user,
        prize: bodyRequest.prize
    }
    prizeHistoryModel.create(createPrizeHistory, (error, data) => {
        if (error) {
            return response(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: create prize history successfully",
                data: data
            })
        }
    })
}


// LẤY TẤT CẢ PRIZE HISTORY
const getAllPrizeHistory = (request, response) => {
    //B1: thu thập dữ liệu
    let user = request.query.user;

    let condition = {};

    if (user) {
        condition.user = user
    }
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    prizeHistoryModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all prize history successfully",
                data: data
            })
        }
    })
}

// LẤY PRIZE  HISTORY THEO ID
const getPrizeHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let prizeHistoryId = request.params.prizeHistoryId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Prize History Id is not a valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    prizeHistoryModel.findById(prizeHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get prize History by Id successfully",
                data: data
            })
        }
    })
}


//UPDATE A PRIZE HISTORY
const updatePrizeHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let prizeHistoryId = request.params.prizeHistoryId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Prize History ID is not a valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updatePrizeHistory = {
        user: bodyRequest.user,
        prize: bodyRequest.prize
    }
    prizeHistoryModel.findByIdAndUpdate(prizeHistoryId, updatePrizeHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal Server Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update Prize History By Id Success",
                data: data
            })
        }
    })
}


// DELETE A PRIZE HISTORY
const deletePrizeHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let prizeHistoryId = request.params.prizeHistoryId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Prize History ID is not a valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    prizeHistoryModel.findByIdAndDelete(prizeHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete Prize History By ID successfully deleted"
            })
        }
    })

}




// EXPORT
module.exports = { createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById }