//Khai báo thư viện express
const express = require('express');

//Import Middleware
const { voucherMiddleware } = require('../middleware/voucherMiddleware');

//Import Controller
const { createVoucher, getAllVoucher, getVoucherById, updateVoucher, deleteVoucherById } = require('../controller/voucherController')

//Tạo Router
const voucherRouter = express.Router();

//Sử dụng middleware
voucherRouter.use(voucherMiddleware);

//Khai báo API

//PORT
voucherRouter.post('/vouchers', createVoucher);

//GET ALL
voucherRouter.get('/vouchers', getAllVoucher);

//GET BY ID
voucherRouter.get('/vouchers/:voucherId', getVoucherById);

//UPDATE BY ID
voucherRouter.put('/vouchers/:voucherId', updateVoucher);


//DELETE BY ID
voucherRouter.delete('/vouchers/:voucherId', deleteVoucherById);
// EXPORT
module.exports = voucherRouter;



// {
//     "code":"abc",
//     "discount":20,
//     "node":"123abc"
// }